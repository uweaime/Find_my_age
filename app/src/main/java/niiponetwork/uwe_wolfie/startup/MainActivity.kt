package niiponetwork.uwe_wolfie.startup

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    }

    fun btnProcessAgeEvent(view:View){

        val year:Int = enterAge.text.toString().toInt()
        val currentYear = Calendar.getInstance().get(Calendar.YEAR)
        val age = currentYear - year
        txtDisplayAge.text = "You are is $age years old"
    }
}
